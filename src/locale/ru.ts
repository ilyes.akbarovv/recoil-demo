export default {
  translation: {
    edit: "Редактировать",
    create: "Создать",
    cancel: "Отмена",
    save: "Сохранить",
    create_post: "Создать пост",
    editor_title: "Выберите пост или создайте новый",
    title: "Заголовок",
    text: "Текст",
  },
};
