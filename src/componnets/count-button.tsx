import { useSetRecoilState } from "recoil";
import { counterState } from "../state/counter";

export function CountButton() {
  const setCount = useSetRecoilState(counterState);

  const handleClick = () => {
    setCount((prev) => prev + 1);
  };

  return <button onClick={handleClick}>Click Me</button>;
}
