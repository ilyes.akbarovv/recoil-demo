import React, { useState, useEffect } from "react";
import { useTranslation } from "react-i18next";
import { Input } from "../Input/Input";
import { TextArea } from "../TextArea/TextArea";
import { Post } from "../../types";

import s from "./PostForm.module.css";

let id = 1000;

type Props = {
  onSubmit: (post: Post) => void;
  initialValue?: Post;
  onCancel: () => void;
};

export function PostForm({ initialValue, onCancel, onSubmit }: Props) {
  const [title, setTittle] = useState("");
  const [body, setBody] = useState("");
  const { t } = useTranslation();

  const onChangeTitle = (e: React.ChangeEvent<HTMLInputElement>) => {
    setTittle(e.target.value);
  };

  const onChangeBody = (e: React.ChangeEvent<HTMLTextAreaElement>) => {
    setBody(e.target.value);
  };

  const handleSubmit = (e: React.FormEvent) => {
    e.preventDefault();
    id += 1;
    onSubmit({
      title,
      body,
      userId: 1,
      id,
    });
  };

  useEffect(() => {
    if (initialValue) {
      setTittle(initialValue.title);
      setBody(initialValue.body);
    }
  }, [initialValue]);

  return (
    <form onSubmit={handleSubmit}>
      <Input
        type="text"
        placeholder={t("title") as string}
        className={s.titleInput}
        value={title}
        onChange={onChangeTitle}
      />

      <TextArea
        placeholder={t("text") as string}
        value={body}
        onChange={onChangeBody}
      />

      <div className={s.buttonWrap}>
        <button type="button" onClick={onCancel}>
          Отмена
        </button>
        <button type="submit">{t("save")}</button>
      </div>
    </form>
  );
}
