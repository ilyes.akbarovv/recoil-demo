import { memo } from "react";
import { useTranslation } from "react-i18next";
import { Post } from "../../types";
import s from "./PostCard.module.css";

type Props = {
  post: Post;
  isSelected?: boolean;
  onSelect: (post: Post) => void;
};

export const PostCard = memo(
  ({ post, isSelected = false, onSelect }: Props) => {
    const { t } = useTranslation();

    const onClick = () => {
      onSelect(post);
    };

    return (
      <article className={s.card}>
        <p>{post.title}</p>
        <div className={s.buttonContainer}>
          <button type="button" onClick={onClick} disabled={isSelected}>
            {t("edit")}
          </button>
        </div>
      </article>
    );
  }
);
