import { useQuery } from "react-query";
import { useRecoilState, useRecoilValue } from "recoil";
import { selectedPostState, isNewPostSelector } from "../../state/posts";
import { getPosts } from "../../api/posts";
import { PostCard } from "../PostCard/PostCard";
import { POST_REQUEST_KEY } from "../../constants";

import s from "./PostList.module.css";

export function PostList() {
  const { isLoading, data, error } = useQuery({
    queryKey: POST_REQUEST_KEY,
    queryFn: getPosts,
    cacheTime: Infinity,
  });
  const [selectedPost, setSelectedPost] = useRecoilState(selectedPostState);
  const isNewPost = useRecoilValue(isNewPostSelector);

  if (isLoading) {
    return <p>Loading...</p>;
  }

  if (error) {
    return <p>Error:(</p>;
  }

  if (!data) {
    return <p>No data</p>;
  }

  return (
    <div className={s.list}>
      {isNewPost && <div className={s.overlay} />}
      <ul>
        {data.map((post) => (
          <li key={post.id} className={s.listItem}>
            <PostCard
              post={post}
              onSelect={setSelectedPost}
              isSelected={!!selectedPost && selectedPost.id === post.id}
            />
          </li>
        ))}
      </ul>
    </div>
  );
}
