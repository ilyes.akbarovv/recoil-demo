import { ReactNode } from "react";
import s from "./Side.module.css";

type Props = {
  children: ReactNode;
};

export function Side({ children }: Props) {
  return <div className={s.side}>{children}</div>;
}
