import React from "react";
import { useRecoilValue } from "recoil";
import { counterMessageSelector } from "../state/counter";

export function CountText() {
  const text = useRecoilValue(counterMessageSelector);
  return <p>{text}</p>;
}
