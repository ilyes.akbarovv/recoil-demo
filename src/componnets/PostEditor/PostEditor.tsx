import { useRecoilState } from "recoil";
import { useTranslation } from "react-i18next";
import { selectedPostState, isNewPostState } from "../../state/posts";
import { PostForm } from "../PostForm/PostForm";
import { Post } from "../../types";

type Props = {
  onCreatePost: (post: Post) => void;
  onUpdatePost: (post: Post) => void;
};

export function PostEditor({ onCreatePost, onUpdatePost }: Props) {
  const [selectedPost, setSelectedPost] = useRecoilState(selectedPostState);
  const [isNewPost, setNewPost] = useRecoilState(isNewPostState);
  const { t } = useTranslation();

  const createNewPost = () => {
    setNewPost(true);
  };

  const handleCreateNewPost = (post: Post) => {
    onCreatePost(post);
    setNewPost(false);
  };

  const handleUpdatePost = (post: Post) => {
    if (!selectedPost) return;
    onUpdatePost({ ...post, id: selectedPost.id, userId: selectedPost.userId });
    setSelectedPost(null);
  };

  const onClose = () => {
    if (isNewPost) {
      setNewPost(false);
    }
    setSelectedPost(null);
  };

  if (isNewPost) {
    return <PostForm onCancel={onClose} onSubmit={handleCreateNewPost} />;
  }

  if (selectedPost) {
    return (
      <PostForm
        initialValue={selectedPost}
        onCancel={onClose}
        onSubmit={handleUpdatePost}
      />
    );
  }

  return (
    <div>
      <h3>{t("editor_title")}</h3>
      <button type="button" onClick={createNewPost}>
        {t("create_post")}
      </button>
    </div>
  );
}
