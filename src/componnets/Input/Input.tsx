import { InputHTMLAttributes } from "react";
import cs from "classnames";

import s from "./Input.module.css";

export function Input(props: InputHTMLAttributes<HTMLInputElement>) {
  return (
    <div className={s.wrap}>
      <input {...props} className={cs(s.input, props.className)} />
    </div>
  );
}
