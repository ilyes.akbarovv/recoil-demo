import { TextareaHTMLAttributes } from "react";
import cs from "classnames";
import s from "./TextArea.module.css";

export function TextArea(props: TextareaHTMLAttributes<HTMLTextAreaElement>) {
  return (
    <div className={s.wrap}>
      <textarea {...props} className={cs(s.textArea, props.className)} />
    </div>
  );
}
