import axios from "axios";
import { Post } from "../types";

export const getPosts = () =>
  axios
    .get<Post[]>("https://jsonplaceholder.typicode.com/posts")
    .then((response) => response.data);
