import { useCallback } from "react";
import { RecoilRoot } from "recoil";
import { QueryClientProvider } from "react-query";
import { Side } from "./componnets/Side/Side";
import { PostList } from "./componnets/PostList/PostList";
import { PostEditor } from "./componnets/PostEditor/PostEditor";
import { queryClient } from "./api/query-client";

import s from "./App.module.css";
import { Post } from "./types";
import { POST_REQUEST_KEY } from "./constants";

function App() {
  const onCreatePost = useCallback((post: Post) => {
    queryClient.setQueryData<Post[]>(POST_REQUEST_KEY, (posts) =>
      !!posts ? [post, ...posts] : []
    );
  }, []);

  const onUpdatePost = useCallback((post: Post) => {
    queryClient.setQueryData<Post[]>(POST_REQUEST_KEY, (posts) => {
      if (!posts) return [];

      const postIndex = posts.findIndex((p) => p.id === post.id);

      if (postIndex === -1) return posts;

      const newPosts = [...posts];

      newPosts[postIndex] = post;

      return newPosts;
    });
  }, []);

  return (
    <QueryClientProvider client={queryClient}>
      <RecoilRoot>
        <main className={s.container}>
          <section>
            <Side>
              <PostList />
            </Side>
          </section>
          <section>
            <Side>
              <PostEditor
                onCreatePost={onCreatePost}
                onUpdatePost={onUpdatePost}
              />
            </Side>
          </section>
        </main>
      </RecoilRoot>
    </QueryClientProvider>
  );
}

export default App;
