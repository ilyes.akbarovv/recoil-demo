import { atom, selector } from "recoil";

export const counterState = atom<number>({
  key: "counterState",
  default: 0,
});

export const counterMessageSelector = selector({
  key: "counterMessageSelector",
  get: ({ get }) => {
    const count = get(counterState);
    return `Количество кликов: ${count}`;
  },
});
