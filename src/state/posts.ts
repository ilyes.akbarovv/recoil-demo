import { atom, selector } from "recoil";
import { Post } from "../types";

type SelectedPostState = Post | null;

export const selectedPostState = atom<SelectedPostState>({
  key: "selectedPost",
  default: null,
});

export const selectedPostSelector = selector<SelectedPostState>({
  key: "selectedPostSelector",
  get: ({ get }) => {
    return get(selectedPostState);
  },
});

export const isNewPostState = atom<boolean>({
  key: "isNewPost",
  default: false,
});

export const isNewPostSelector = selector<boolean>({
  key: "isNewPostSelector",
  get: ({ get }) => {
    return get(isNewPostState);
  },
});
